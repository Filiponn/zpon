package main;

import main.CustomerTokenDecoder;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTokenDecoderTest {
    private CustomerTokenDecoder cTD;

    @org.junit.jupiter.api.Test
    void decodeToken() {
        cTD = new CustomerTokenDecoder();
        Assertions.assertEquals("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1",
                cTD.decodeToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJjdXN0b21lclRva2VuIjoiUEw0MzI1ZXhfIGZiYTExMGQ" +
                "wLTQ4NDgtNGRjMC05ZThkLWIxMTQxY2ZkZDY0ZTEyLjQzLjU0LjYuMi4xIiwibmFtZSI6IlBXciIsImlhdCI6MTUxNjIzOT" +
                "AyMn0.ikK6v4yFhXnBf5M9ZYJi1cBVajEnwuu9dgYuWZYYE7g"));
    }

    @org.junit.jupiter.api.Test
    void decode() throws Exception {
        cTD = new CustomerTokenDecoder();
        Assertions.assertEquals("domain.Customer{countryCode='PL', id='4325', externalId='fba110d0-4848-4dc0-9e8d-b1141cfdd64e'," +
                        " enabledServices='[12, 43, 54, 6, 2, 1]'}",
                cTD.decode("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"));
    }

    @Test
    public void exceptionDecodeMethod(){
        cTD = new CustomerTokenDecoder();
        try {
            cTD.decodeToken("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ikpv" +
                    "aG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");
            fail("Nie wyrzucono wyjątku!");
        }
        catch (IllegalStateException exception) {
            assertEquals("Nie znaleziono customerToken!", exception.getMessage());
        }
    }

    @Test
    public void shouldThrownIllegalStateException() throws Exception {
        cTD = new CustomerTokenDecoder();
        try {
            cTD.decode("eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6Ikpv" +
                    "aG4gRG9lIiwiaWF0IjoxNTE2MjM5MDIyfQ.SflKxwRJSMeKKF2QT4fwpMeJf36POk6yJV_adQssw5c");
            fail("Nie wyrzucono wyjątku!");
        }
        catch (IllegalStateException exception) {
            assertEquals("Niepoprawny format CustomerToken!", exception.getMessage());
        }
    }
}