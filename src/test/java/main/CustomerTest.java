package main;

import domain.CountryCode;
import domain.Customer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.UUID;

class CustomerTest {
    Customer customer;
    CountryCode countryCode = new CountryCode("PL");
    UUID uuid = UUID.fromString("fba110d0-4848-4dc0-9e8d-b1141cfdd64e");
    ArrayList<Integer> id = new ArrayList<>();

    CustomerTest() throws Exception {
        id.add(12);
        id.add(43);
        id.add(54);
        id.add(6);
        id.add(2);
        id.add(1);
    }

    @Test
    void getEnabledServices() {
        customer = new Customer(countryCode,4325,uuid,id);
        Assertions.assertEquals("[12, 43, 54, 6, 2, 1]",
                customer.getEnabledServices().toString());
    }

    @Test
    void toStringTest() {
        customer = new Customer(countryCode,4325,uuid,id);
        Assertions.assertEquals("domain.Customer{countryCode='PL', id='4325', externalId='fba110d0-4848-4dc0-9e8d-b1141cfdd64e', enabledServices='[12, 43, 54, 6, 2, 1]'}",
                customer.toString());
    }
}