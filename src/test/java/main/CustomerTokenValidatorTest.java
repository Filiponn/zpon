package main;

import main.CustomerTokenValidator;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CustomerTokenValidatorTest {
    private CustomerTokenValidator cTV;

    @Test
    void customerTokenValidator() throws Exception {
        cTV = new CustomerTokenValidator();
        Assertions.assertEquals("domain.Customer{countryCode='PL', id='4325', externalId='fba110d0-4848-4dc0-9e8d-b1141cfdd64e', enabledServices='[12, 43, 54, 6, 2, 1]'}",
                cTV.customerTokenValidatorMethod(12, "PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1"));
    }

    @Test
    public void shouldThrownNullPointerException() {
        cTV = new CustomerTokenValidator();
        try {
            cTV.customerTokenValidatorMethod(13, "PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1");
            fail("Nie wyrzucono wyjątku!");
        } catch (Exception exception) {
            assertEquals("Niepoprawny ID! Nie masz dostępu do serwisu!", exception.getMessage());
        }
    }
}