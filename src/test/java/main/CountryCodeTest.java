package main;

import domain.CountryCode;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CountryCodeTest {
     CountryCode countryCode;

    @Test
    void setCode() {

        try {
            countryCode = new CountryCode("WWA");
            fail("Nie wyrzucono wyjątku!");
        } catch (Exception exception) {
            assertEquals("Kod kraju musi się składać z dwóch liter!", exception.getMessage());
        }
    }
}