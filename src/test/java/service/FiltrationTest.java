package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import decryption.Decryption;
import domain.MetaData;
import file.FileManagement;
import domain.Image;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

class FiltrationTest {
    List<Image> images = new ArrayList<>();

    private void setImages(){
        images.add(new Image(null, new MetaData("Filip Kon","Domaniów city","Domaniów",
                LocalDate.of(2000,10,10),"Fajnie","jpg"),0));
        images.add(new Image(null, new MetaData("Paweł Gad","Sauna","Kraków",
                LocalDate.of(2012,10,10),"Supi","jpg"),1));
        images.add(new Image(null, new MetaData("Andrzej Mak","Church","Warszawa",
                LocalDate.of(2020,10,10),"ooo","jpg"),2));
    }

    @Test
    void imagesFiltration() throws Exception {
        setImages();
        Assertions.assertEquals("[Image{photo=null, metaData=MetaData{author='Filip Kon', title='Domaniów city'," +
                        " location='Domaniów', date='2000-10-10', description='Fajnie', format='jpg'}, id=0}]",
                Filtration.imagesFiltration(images,"Domaniów city", "Domaniów",
                        LocalDate.of(1999,02,20),null,"Fajnie","filip Kon").toString());
    }

    @Test
    void readNote() throws Exception {
        JsonElement element = JsonParser.parseString(Decryption.noteDecryption("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1",
                0, "fsd"));
        Assertions.assertEquals("note example\n" +
                        "note example\n" +
                        "note example\n",
                Filtration.readNote(element,"note"));
    }

    @Test
    void readNote2() throws Exception {
        JsonElement element = JsonParser.parseString(Decryption.noteDecryption("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1",
                0, "fsd"));
        Assertions.assertEquals("note example\n" +
                        "eeeE?\n" +
                        "co to kurk\n" +
                        "note example\n" +
                        "fajne nawet\n" +
                        "note example\n" +
                        "nie mam ich\n",
                Filtration.readNote(element,null));
    }
}