package encryption;

import aes.AESUtil;
import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;
import decryption.Decryption;
import file.FileManagement;
import domain.Customer;
import main.CustomerTokenDecoder;
import domain.MetaData;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.json.JSONObject;
import service.Collection;

import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Base64;

public class Encryption {

    public static String mainKeyEncryption(String mainKey, String password, String customerToken) throws Exception {
        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

        IvParameterSpec ivMainKey = AESUtil.generateIv();
        String algorithm = "AES/CBC/PKCS5Padding";

        String salt = Collection.getSmthFromCollection(customer.getId(),"key");

        String mainKeyEncrypted = null;
        if (salt.equals("null")) {
            String saltNew = AESUtil.getNextSalt();
            SecretKey secretKey = AESUtil.getKeyFromPassword(password, saltNew);
            System.out.println(AESUtil.convertSecretKeyToString(secretKey));
            mainKeyEncrypted = AESUtil.encrypt(algorithm, mainKey, secretKey, ivMainKey);
            String encodedIV = Base64.getEncoder().encodeToString(ivMainKey.getIV());
            // Dodanie do kolekcji customera z key i salt
            Collection.addCustomer(customer.getId(), encodedIV, saltNew, mainKeyEncrypted);
        }
        return mainKeyEncrypted;
    }

    public static String imageEncryption(String imageEncoded, String password, String customerToken) throws Exception {
        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

//        Base64.Decoder decoder = Base64.getUrlDecoder();
//        byte[] bytes = decoder.decode(stringIV);
        IvParameterSpec ivParameterSpec = AESUtil.generateIv();
        String algorithm = "AES/CBC/PKCS5Padding";

        String mainKeyDecrypted = Decryption.mainKeyDecryption(customerToken,password);

        String cipherText = AESUtil.encrypt(algorithm, imageEncoded,
                AESUtil.convertStringToSecretKeyto(mainKeyDecrypted), ivParameterSpec);

        // domyślne id dla zdjęcia
        int imageID = 0;

        // Sprawdzenie, czy dany plik istnieje, jeśli tak to zmiana nazwy pliku (imageID)
        while (FileManagement.findIfFileExists("data/" + customer.getId(), "p" + imageID + ".txt")) {
            imageID++;
        }
        FileManagement.writeToFile("p" + imageID + ".txt", "data/"
                + customer.getId(), cipherText);

        String encodedIV = Base64.getEncoder().encodeToString(ivParameterSpec.getIV());

        FileManagement.writeToFile("ivI" + imageID + ".txt", "data/"
                + customer.getId(), encodedIV);
        return cipherText;
    }

    public static String metaDataEncryption(String path, String customerToken, String password) throws Exception {
        JSONParser jsonParser = new JSONParser(JSONParser.MODE_JSON_SIMPLE);

        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

        IvParameterSpec ivParameterSpec = AESUtil.generateIv();
        String encodedIV = Base64.getEncoder().encodeToString(ivParameterSpec.getIV());

        String mainKeyDecrypted = Decryption.mainKeyDecryption(customerToken,password);

        JSONObject jsonObject = null;
        String cipherText = null;
        try {
            FileReader reader = new FileReader(path);
            Object obj = jsonParser.parse(reader);

            jsonObject = new JSONObject(obj.toString());

            String author = (String) jsonObject.get("author");
            String title = (String) jsonObject.get("title");
            String location = (String) jsonObject.get("location");
            String date = (String) jsonObject.get("date");
            String description = (String) jsonObject.get("description");
            String format = (String) jsonObject.get("format");


            MetaData metaData = new MetaData(author, title, location, LocalDate.parse(date), description, format);
            XStream xstream = new XStream(new DomDriver());

            String xml = xstream.toXML(metaData);
            cipherText = AESUtil.encrypt("AES/CBC/PKCS5Padding", xml,
                    AESUtil.convertStringToSecretKeyto(mainKeyDecrypted), ivParameterSpec);

            int metaDataID = 0;

            while (FileManagement.findIfFileExists("data/" + customer.getId(), "md" +
                    metaDataID + ".txt")) {
                metaDataID++;
            }

            FileManagement.writeToFile("md" + metaDataID + ".txt", "data/" + customer.getId(), cipherText);
            FileManagement.writeToFile("ivMD" + metaDataID + ".txt", "data/"
                    + customer.getId(), encodedIV);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return cipherText;
    }

    public static String noteEncryption(String path, String customerToken, String password) throws Exception {
        JSONParser jsonParser = new JSONParser(JSONParser.MODE_JSON_SIMPLE);

        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

        IvParameterSpec ivParameterSpec = AESUtil.generateIv();
        String encodedIV = Base64.getEncoder().encodeToString(ivParameterSpec.getIV());

        String mainKeyDecrypted = Decryption.mainKeyDecryption(customerToken,password);

        JSONObject jsonObject = null;
        String cipherText = null;
        try {
            FileReader reader = new FileReader(path);
            Object obj = jsonParser.parse(reader);

            jsonObject = new JSONObject(obj.toString());

            cipherText = AESUtil.encrypt("AES/CBC/PKCS5Padding", jsonObject.toString(),
                    AESUtil.convertStringToSecretKeyto(mainKeyDecrypted), ivParameterSpec);

            int noteID = 0;

            while (FileManagement.findIfFileExists("data/" + customer.getId(), "note" +
                    noteID + ".txt")) {
                noteID++;
            }

            FileManagement.writeToFile("note" + noteID + ".txt", "data/" + customer.getId(), cipherText);
            FileManagement.writeToFile("ivNote" + noteID + ".txt", "data/"
                    + customer.getId(), encodedIV);
        } catch (IOException | ParseException e) {
            e.printStackTrace();
        }
        return cipherText;
    }

}
