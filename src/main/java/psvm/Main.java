package psvm;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import decryption.Decryption;
import encoding.Encoding;
import encryption.Encryption;
import file.FileManagement;
import domain.Image;
import service.Filtration;

import java.time.LocalDate;
import java.util.List;


public class Main {
    public static void main(String[] args) throws Exception {
//        String pathMetaData = "src/main/java/service/meta_data1.json";
//        String pathImage = "trunk.png";
//        String encodedImage = Encoding.imageEncoding(pathImage);
//        String encryptedImage = Encryption.imageEncryption(encodedImage,
//                "fsd","PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1");
//        String encryptedMetaData = Encryption.metaDataEncryption(pathMetaData,
//                "PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1","fsd");
//        System.out.println("Przed odszyfrowaniem: " + encryptedMetaData);
//        byte[] decryptedImage = Decryption.imageDecryption("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1",
//                2,"fsd");
//        String decryptedMetaData = Decryption.metaDataDecryption("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1",
//                2,"fsd");
//        Image.byteArrayToImage(decryptedImage,"test.png");
//        System.out.println("Po odszyfrowaniu: "+decryptedMetaData);


//        String pathNote = "src/main/java/service/note.json";
//        String encryptedNote = Encryption.noteEncryption(pathNote, "PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1",
//                "fsd");
//        System.out.println("Przed odszyfrowaniem notatki");
//        System.out.println(encryptedNote);
//        String decryptedNote = Decryption.noteDecryption("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1", 0,
//                "fsd");
//        System.out.println("Po odszyfrowaniu notatki");
//        System.out.println(decryptedNote);


//        System.out.println("Filtracja");
//        List<Image> images = FileManagement.pathFinding("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1", "fsd");
//        List<Image> filteredImages = Filtration.imagesFiltration(images,"Malinówek", "malinówek",
//                LocalDate.of(1999,02,20),null,"Jez","adam, krzysztof");
//        System.out.println(filteredImages);
//        List<Image> filteredImages1 = Filtration.imagesFiltration(images,"wwa vibe", "Warszawa",
//                LocalDate.of(2010,02,20),LocalDate.of(2020,02,20),"ryne","filip bak");
//        System.out.println(filteredImages1);

        JsonElement element = JsonParser.parseString(Decryption.noteDecryption("PL4325ex_ fba110d0-4848-4dc0-9e8d-b1141cfdd64e12.43.54.6.2.1",
                0, "fsd"));
        System.out.println(element);
        System.out.println(Filtration.readNote(element,null));
    }
}
