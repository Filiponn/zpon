package encoding;
import domain.Image;
import java.io.IOException;
import java.util.Base64;

public class Encoding {

    public static String imageEncoding(String pathImage) throws IOException {
        byte[] bytes = Image.imageToByteArray(pathImage);
        return Base64.getEncoder().encodeToString(bytes);
    }
}
