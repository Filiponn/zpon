package domain;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Arrays;

public class Image {

    private byte[] photo;
    private MetaData metaData;
    private int id;

    public Image(byte[] photo, MetaData metaData, int id) {
        this.photo = photo;
        this.metaData = metaData;
        this.id = id;
    }

    @Override
    public String toString() {
        return "Image{" +
                "photo=" + photo +
                ", metaData=" + metaData +
                ", id=" + id +
                '}';
    }

    public byte[] getPhoto() {
        return photo;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static byte[] imageToByteArray(String path) throws IOException {
        File fi = new File(path);
        byte[] fileContent = Files.readAllBytes(fi.toPath());
        return fileContent;
    }

    public static void byteArrayToImage(byte[] image, String path) throws IOException {
        InputStream is = new ByteArrayInputStream(image);
        BufferedImage newBi = ImageIO.read(is);

        Path target = Path.of(path);
        ImageIO.write(newBi, "png", target.toFile());
    }
}
