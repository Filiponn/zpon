package domain;

import java.util.ArrayList;
import java.util.UUID;

public class Customer {
    private CountryCode countryCode;
    private int id;
    private UUID externalId;
    private ArrayList<Integer> enabledServices;

    public Customer(CountryCode countryCode, int id, UUID externalId, ArrayList<Integer> enabledServices) {
        this.countryCode = countryCode;
        this.id = id;
        this.externalId = externalId;
        this.enabledServices = enabledServices;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public UUID getExternalId() {
        return externalId;
    }

    public void setExternalId(UUID externalId) {
        this.externalId = externalId;
    }

    public void setEnabledServices(ArrayList<Integer> enabledServices) {
        this.enabledServices = enabledServices;
    }

    public ArrayList<Integer> getEnabledServices() {
        return enabledServices;
    }

    @Override
    public String toString() {
        return "domain.Customer{" +
                "countryCode='" + countryCode + '\'' +
                ", id='" + id + '\'' +
                ", externalId='" + externalId + '\'' +
                ", enabledServices='" + enabledServices + '\'' +
                '}';
    }
}
