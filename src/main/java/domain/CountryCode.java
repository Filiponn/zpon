package domain;

public class CountryCode {
    private String code;

    public CountryCode(String code) throws Exception {
        setCode(code);
    }

    public void setCode(String code) throws Exception {
        if (code.length() != 2){
            throw new Exception("Kod kraju musi się składać z dwóch liter!");
        }else
            this.code = code;
    }

    @Override
    public String toString() {
        return code;
    }
}
