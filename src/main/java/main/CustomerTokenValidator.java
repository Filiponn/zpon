package main;

import domain.Customer;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomerTokenValidator {
    public Customer customerTokenValidatorMethod(int id, String customerToken) throws Exception {
        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);
        ArrayList<Integer> totalID = customer.getEnabledServices();
        Pattern pattern = Pattern.compile(".*(12).*");
        Matcher matcher = pattern.matcher(totalID.toString());
        matcher.matches();
        String stringID = matcher.group(1);
        int idToken = Integer.parseInt(stringID);

        if (idToken != id)
            throw new Exception("Niepoprawny ID! Nie masz dostępu do serwisu!");
        return customer;
    }
}
