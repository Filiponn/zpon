package main;

import domain.CountryCode;
import domain.Customer;

import java.util.ArrayList;
import java.util.Base64;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CustomerTokenDecoder {

    public String decodeToken(String token) throws NullPointerException {
        String[] chunks = token.split("\\.");
        Base64.Decoder decoder = Base64.getUrlDecoder();
        String payload = new String(decoder.decode(chunks[1]));
        Pattern pattern = Pattern.compile(".*\"customerToken\":\"([^\"]+).*");
        Matcher matcher = pattern.matcher(payload);
        boolean match = matcher.matches();
        if (match == false){
            throw new IllegalStateException("Nie znaleziono customerToken!");
        }else
            return matcher.group(1);
    }

    public Customer decode(String customerToken) throws Exception {
        Pattern pattern = Pattern.compile("([A-Z]{2})(\\d+)ex_ ([^.]{36})([\\d\\.]+)");
        Matcher matcher = pattern.matcher(customerToken);
        boolean match = matcher.matches();
        if (match == false){
            throw new IllegalStateException("Niepoprawny format CustomerToken!");
        }
        CountryCode countryCode = new CountryCode(matcher.group(1));
        UUID guid = UUID.fromString(matcher.group(3));
        String[] idArray = matcher.group(4).split("\\.");
        ArrayList<Integer> idList = new ArrayList<>();
        for (String id : idArray) {
            idList.add(Integer.parseInt(id.trim()));
        }
        Customer customer = new Customer(countryCode,Integer.parseInt(matcher.group(2)),guid,idList);
        return customer;
    }
}

