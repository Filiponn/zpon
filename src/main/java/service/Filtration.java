package service;

import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import decryption.Decryption;
import domain.Image;
import java.time.LocalDate;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;


public class Filtration {


    public static List<Image> imagesFiltration(List<Image> images, String title, String location,
                                                  LocalDate startDate, LocalDate endDate, String description, String author) throws Exception {

        return images.stream().filter(
                x -> {
                    boolean isAuthorAndLocation = x.getMetaData().getAuthor().equalsIgnoreCase(author) &&
                            x.getMetaData().getLocation().equalsIgnoreCase(location);
                    if (!isAuthorAndLocation) {
                        return false;
                    }
                    boolean isTitleAndDescription = x.getMetaData().getTitle().toLowerCase().contains(title.toLowerCase()) &&
                            x.getMetaData().getDescription().toLowerCase().contains(description.toLowerCase());
                    if (!isTitleAndDescription) {
                        return false;
                    }
                    boolean correctDate = startDate == null && endDate == null;

                    if (startDate != null && endDate != null) {
                        correctDate = x.getMetaData().getDate().isBefore(endDate) && x.getMetaData().getDate().isAfter(startDate);

                    }
                    if (startDate == null && endDate != null) {
                        correctDate = x.getMetaData().getDate().isBefore(endDate);
                    }

                    if (endDate == null && startDate != null) {
                        correctDate = x.getMetaData().getDate().isAfter(startDate);
                    }
                    return correctDate;
                }).collect(Collectors.toList());

    }
    public static String readNote(JsonElement jsonElement, String searchFor){
        StringBuilder notes = new StringBuilder();

        if(jsonElement.isJsonPrimitive()){
            if (searchFor != null){
                Boolean filtered = jsonElement.getAsString().toLowerCase().contains(searchFor.toLowerCase());
                if (filtered)
                    notes.append(jsonElement.getAsString()).append("\n");
            }
            else
                notes.append(jsonElement.getAsString()).append("\n");
        }
        else {
            JsonObject obj = jsonElement.getAsJsonObject();
            for (String key : obj.keySet()) {
                notes.append(readNote(obj.get(key),searchFor));
            }
        }
        return notes.toString();
    }
}
