package service;
import file.FileManagement;
import net.minidev.json.parser.JSONParser;
import net.minidev.json.parser.ParseException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.json.XML;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class Collection {
    public static void addCustomer(int argId, String argIV, String argSalt, String argMainKey)
            throws IOException, SAXException, ParserConfigurationException, TransformerException {

        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.parse("src/main/java/service/collection.xml");

        Element root = document.getDocumentElement();
        Element customers = document.createElement("customer");

        Element id = document.createElement("id");
        id.appendChild(document.createTextNode(String.valueOf(argId)));
        customers.appendChild(id);

        Element key = document.createElement("key");
        key.appendChild(document.createTextNode(argMainKey));
        customers.appendChild(key);

        Element salt = document.createElement("salt");
        salt.appendChild(document.createTextNode(argSalt));
        customers.appendChild(salt);

        Element iv = document.createElement("iv");
        iv.appendChild(document.createTextNode(argIV));
        customers.appendChild(iv);

        root.appendChild(customers);

        DOMSource source = new DOMSource(document);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        StreamResult result = new StreamResult("src/main/java/service/collection.xml");
        transformer.transform(source, result);
    }

    public static String getSmthFromCollection(int id, String keyInCollection) throws FileNotFoundException {
        String xmlToString = FileManagement.xmlToString("src/main/java/service/collection.xml");

        JSONObject jsonObject = XML.toJSONObject(xmlToString);

        JSONObject customers = jsonObject.getJSONObject("customers");
        JSONArray array = customers.getJSONArray("customer");

        for (int n = 0; n < array.length(); n++) {
            JSONObject object = array.getJSONObject(n);
            int customerId = object.getInt("id");
            if (customerId == id)
                return object.getString(keyInCollection);
        }
        return "null";
    }

    public static String getIVFromMD(String path) throws FileNotFoundException {
        JSONParser jsonParser = new JSONParser(JSONParser.MODE_JSON_SIMPLE);
        String iv = null;
        try {
            FileReader reader = new FileReader(path);
            Object obj = jsonParser.parse(reader);

            JSONObject jsonObject = new JSONObject(obj.toString());
            iv = (String) jsonObject.get("IV");

        } catch (ParseException e) {
            e.printStackTrace();
        }
        return iv;
    }

}
