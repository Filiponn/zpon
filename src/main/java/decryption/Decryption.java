package decryption;

import aes.AESUtil;
import decoding.Decoding;
import file.FileManagement;
import domain.Customer;
import main.CustomerTokenDecoder;
import service.Collection;

import javax.crypto.spec.IvParameterSpec;
import java.util.Base64;

public class Decryption {

    public static String mainKeyDecryption(String customerToken, String password) throws Exception {
        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

        String iv = Collection.getSmthFromCollection(customer.getId(),"iv");
        byte[] decodedBytes = Base64.getDecoder().decode(iv);

        IvParameterSpec ivParameterSpec = new IvParameterSpec(decodedBytes);

        String salt = Collection.getSmthFromCollection(customer.getId(), "salt");

        String mainKeyEncrypted = Collection.getSmthFromCollection(customer.getId(), "key");

        return AESUtil.decrypt("AES/CBC/PKCS5Padding", mainKeyEncrypted,
                AESUtil.getKeyFromPassword(password, salt),ivParameterSpec);
    }

    public static byte[] imageDecryption(String customerToken, int imageID, String password) throws Exception {
        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

        String iv = FileManagement.readFile("data/" + customer.getId() + "/ivI" + imageID + ".txt");
        byte[] decodedBytes = Base64.getDecoder().decode(iv);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(decodedBytes);

        String mainKeyDecrypted = Decryption.mainKeyDecryption(customerToken,password);

        return Decoding.imageDecoding(AESUtil.decrypt("AES/CBC/PKCS5Padding", FileManagement.readFile("data/" +
                        customer.getId() + "/p" + imageID + ".txt"),
                AESUtil.convertStringToSecretKeyto(mainKeyDecrypted),ivParameterSpec));
    }

    public static String metaDataDecryption(String customerToken, int metaDataID, String password) throws Exception {
        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

        String iv = FileManagement.readFile("data/" + customer.getId() + "/ivMD" + metaDataID + ".txt");
        byte[] decodedBytes = Base64.getDecoder().decode(iv);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(decodedBytes);

        String mainKeyDecrypted = Decryption.mainKeyDecryption(customerToken,password);

        return AESUtil.decrypt("AES/CBC/PKCS5Padding", FileManagement.readFile("data/" +
                        customer.getId() + "/md" + metaDataID + ".txt"),
                AESUtil.convertStringToSecretKeyto(mainKeyDecrypted),ivParameterSpec);
    }

    public static String noteDecryption(String customerToken, int noteID, String password) throws Exception {
        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

        String iv = FileManagement.readFile("data/" + customer.getId() + "/ivNote" + noteID + ".txt");
        byte[] decodedBytes = Base64.getDecoder().decode(iv);
        IvParameterSpec ivParameterSpec = new IvParameterSpec(decodedBytes);

        String mainKeyDecrypted = Decryption.mainKeyDecryption(customerToken,password);

        return AESUtil.decrypt("AES/CBC/PKCS5Padding", FileManagement.readFile("data/" +
                        customer.getId() + "/note" + noteID + ".txt"),
                AESUtil.convertStringToSecretKeyto(mainKeyDecrypted),ivParameterSpec);
    }
}
