package decoding;

import java.io.IOException;
import java.util.Base64;

public class Decoding {
    public static byte[] imageDecoding(String imageEncoded) throws IOException {
        return Base64.getDecoder().decode(imageEncoded);
    }
}
