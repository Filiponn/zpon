package file;

import decryption.Decryption;
import domain.Image;
import domain.Customer;
import main.CustomerTokenDecoder;
import domain.MetaData;
import org.json.JSONObject;
import org.json.XML;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDate;
import java.util.*;
import java.util.stream.Stream;

public class FileManagement {

    public static String xmlToString(String path) throws FileNotFoundException {
        File file = new File(path);
        Scanner in = new Scanner(file);
        StringBuilder fileString = new StringBuilder();
        Boolean nLine = true;
        while (in.hasNext()) {
            String x = in.nextLine();
            fileString.append(x);
            if (nLine)
                fileString.append("\n");
        }
        return String.valueOf(fileString);
    }

    public static void writeToFile(String filename, String fileDirectory, String encryptedString) throws IOException {
        // Tworzy katalog o id customer w data, jeśli nie istnieje
        Files.createDirectories(Paths.get(fileDirectory));
        PrintWriter printWriter = new PrintWriter(fileDirectory + "/" + filename);
        printWriter.println(encryptedString);
        printWriter.close();
    }

    public static String readFile(String path) throws FileNotFoundException {
        File file = new File(path);
        Scanner sc = new Scanner(file);
        StringBuilder read = new StringBuilder();
        while (sc.hasNextLine())
            read.append(sc.nextLine());
        return read.toString();
    }

    public static boolean findIfFileExists(String fileDirectory, String fileName) {
        File tempFile = new File(fileDirectory + "/" + fileName);

        boolean exists = tempFile.exists();
        return exists;
    }
    
    public static List<Image> pathFinding(String customerToken, String password) throws Exception {
        CustomerTokenDecoder c = new CustomerTokenDecoder();
        Customer customer = c.decode(customerToken);

        List<Image> allImages = new ArrayList<>();

        List<Path> listMeta;
        int depth = Integer.MAX_VALUE;
        try (Stream<Path> paths = Files.find(
                Path.of("data/" + customer.getId() + "/"),
                depth,
                (path, attr) -> attr.isRegularFile() && path.toString().startsWith("data\\4325\\m"))) {
            listMeta = paths.toList();
        }

        List<Path> listPhoto;
        try (Stream<Path> paths = Files.find(
                Path.of("data/" + customer.getId() + "/"),
                depth,
                (path, attr) -> attr.isRegularFile() && path.toString().startsWith("data\\4325\\p"))) {
            listPhoto = paths.toList();
        }
        List<String> metaDateList = new ArrayList<>();
        List<byte[]> photoList = new ArrayList<>();

        listMeta.forEach(x -> {
            try {
                metaDateList.add(Decryption.metaDataDecryption(customerToken, listMeta.indexOf(x), password));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        listPhoto.forEach(x -> {
            try {
                photoList.add(Decryption.imageDecryption(customerToken, listPhoto.indexOf(x), password));
            } catch (Exception e) {
                e.printStackTrace();
            }
        });

        for (int i = 0; i < listMeta.size(); i++) {
            Image newIm = new Image(photoList.get(i), getSmthFromMetadata(metaDateList.get(i)), i);
            allImages.add(newIm);
        }

        return allImages;
    }

    public static MetaData getSmthFromMetadata(String xml) throws FileNotFoundException {

        JSONObject jsonObject = XML.toJSONObject(xml);

        JSONObject metadata = jsonObject.getJSONObject("domain.MetaData");

        MetaData metaData = new MetaData(metadata.getString("author"), metadata.getString("title"),
                metadata.getString("location"), LocalDate.parse(metadata.getString("date")),
                metadata.getString("description"), metadata.getString("format"));

        return metaData;
    }

}



